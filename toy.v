(* 1. Language *)
Inductive toy :=
| IfThenElse (case: bool) (tbranch: toy) (fbranch: toy)
| Const
| DoFibonacci100.

Fixpoint fibonacci_bad (n: nat) :=
  match n with
  | 0 => 0
  | S 0 => 1
  | S ((S n'') as n') => fibonacci_bad n'' + fibonacci_bad n'
  end.

Fixpoint eval_toy (t : toy) : nat :=
 match t with
 | IfThenElse case tbranch fbranch =>
   if case
   then eval_toy tbranch
   else eval_toy fbranch
 | Const => 0
 | DoFibonacci100 => (* Might be a bit slow on older hardware *)
   fibonacci_bad 100
 end.

(* 2. Examples *)
(* 2.1. Nice example *)
Definition nice_program := IfThenElse true Const DoFibonacci100.
Lemma nice_program_returns_zero : eval_toy nice_program = 0.
Proof. reflexivity. Qed. (* Very nice! *)

(* 2.2. Sad example *)
Axiom mysterious_variable : bool.
Axiom mysterious_variable_is_true : mysterious_variable = true.
Definition bad_program :=
  IfThenElse
    true
    (IfThenElse mysterious_variable Const DoFibonacci100)
    Const.

Lemma bad_program_returns_zero : eval_toy bad_program = 0.
Proof.
  (* This one is more complex so let's unfold it instead of going in blind. *)
  unfold bad_program.
  (* Notice how the external IfThenElse can be simplified: *)
  Fail Timeout 5 simpl. (* Oh no, simplification blew up >:( *)
  (* "cbn" is supposed to be a more efficient tactic than "simpl" for
     simplification. Let's try using it instead: *)
  Fail Timeout 5 cbn. (* Come on >:( *)
  (* For the record, "cbv" and friends overflow. *)

  (* Of course, we can still prove this lemma by other means. *)
  rewrite mysterious_variable_is_true.
  reflexivity.
  (* This works, but it is somewhat sad:
     * What if we were students learning Coq and getting frustrated at how we
       cannot simplify this example as we intuitively should be able to?
     * What if we wanted to prove properties about a 5000 lines long program
       written in a real-world language, and if we had some assumptions we could
       use to get it down to a fraction of that size?

     Wouldn't it be nice if we had more controllable and predictable ways of
     simplifying such expressions in Coq? *)
Qed.
